/*
 * Alexander Alvara
 * alvaraa88
 * 11/10/20
 */

/*
• -h: Optional help flag that prints usage info
• -v: Optional verbose flag that displays trace info
• -s <s>: Number of set index bits (S = 2^s is the number of sets)
• -E <E>: Associativity (number of lines per set)
• -b <b>: Number of block bits (B = 2^b is the block size)
• -t <tracefile>: Name of the valgrind trace to replay
 */
/*
 * Memory size (M) = 64 bits --> 6 bits because s^m = 2^6 = 64bits therefore. m = 6.
 * Memory BLOCK size = b(2^b) --> example: B = 16  therefore b = 4 because 2^b = 2^4 = 16.
 * Cache size = S * E * B --> 4(bytes per word) * E(line size) * S(sets)
 * Associativity = E example: if E = 1. 1 line per set.
 * Number of sets = S(2^s) example S = 4  therefore s = 2 because 2^s = 2^2 = 4
 * tag = m - s - b. example --> 6 - 2 - 4 = 0.
 */

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>


void printSummary(int hits,  /* number of  hits */
                  int misses, /* number of misses */
                  int evictions); /* number of evictions */

bool areArgumentsValid(int numberOfArguments, char **argv);

bool numArgsIs9(char **argv);

bool numArgsIs10(char **argv);

bool isNumArgs9(int numberOfArguments);

bool isNumArgs10(int numberOfArguments);

bool isArgHVCorrect(int index, char **argv);

bool isArgSCorrect(int index, char **argv);

bool isArgECorrect(int index, char **argv);

bool isArgBCorrect(int index, char **argv);

bool isArgTCorrect(int index, char **argv);

void setS_Value(int index, char **argv);

long getS_Value();

long getS_bitSize();

void setE_Value(int index, char **argv);

long getE_Value();

void setB_Value(int index, char **argv);

long getB_Value();

long getB_bitSize();

void setM_Value();

long getM_Value();

void setTag_bitSize();

long getTag_bitSize();

void setFileArgValue(int index);

int getFileArgValue();

void setFile_Operation(char file_operation);

char getFile_Operation();

void setFile_Address(unsigned long file_address);

unsigned long getFile_Address();

void setFile_ByteSize(int file_byteSize);

int getFile_ByteSize();

void setValidBit(int validbit);

int getValidBit();

void setFile_TagBitsAddress(unsigned long file_address);

unsigned long getFile_TagBitsAddress();

void setFile_SetBitsAddress(unsigned long file_address);

unsigned long getFile_SetBitsAddress();

void setFile_BlockBitsAddress(unsigned long file_address);

unsigned long getFile_BlockBitsAddress();

char *readFile(const char *filename);

void printSummary(int hit, int miss, int evict);


/*
 * note: arg 0 is program, arg1 is optional therefore..
 * ./csim-ref [-hv] -s 4 -E 1 -b 4 -t traces/yi.trace = 8 or 9 arguments.... not sure why C is adding an extra arg.
 * assuming: -h and -v cant be used at the same time.
 * This function will check to see if all our arguments are valid.
 * argc should be used as the default numberOfArguments as by definition: it is the # of arguments used.
 * it's split up into smaller portions of code for readability purposes and to expand for future use.
 */
bool areArgumentsValid(int numberOfArguments, char **argv) {

    char errorString[] = "Arguments/File need to be checked.\n";

    if (isNumArgs9(numberOfArguments) == true) {
        numArgsIs9(argv);
        return true;
    } else if (isNumArgs10(numberOfArguments) == true) {
        numArgsIs10(argv);
        return true;
    } else
        printf("%s\n", errorString);
    return false;
}
/*
 * there are 8 arguments in this function.
 * note: ./csim-ref(0) -s(1) 4(2) -E(3) 1(4) -b(5) 4(6) -t(7) traces/yi.trace(8) (9)??null??
 */
bool numArgsIs9(char **argv) {
    setM_Value();
    if (isArgSCorrect(1, argv) == false) {
        printf("-s was typed incorrectly\n");
        return false;
    }
    if (isArgSCorrect(1, argv) != false) {
        setS_Value(2, argv);
    }
    if (isArgECorrect(3, argv) == false) {
        printf("-E was typed incorrectly\n");
        return false;
    }
    if (isArgECorrect(3, argv) != false) {
        setE_Value(4, argv);
    }
    if (isArgBCorrect(5, argv) == false) {
        printf("-b was typed incorrectly\n");
        return false;
    }
    if (isArgBCorrect(5, argv) != false) {
        setB_Value(6, argv);
    }
    if (isArgTCorrect(7, argv) == false) {
        printf("-t was typed incorrectly\n");
        //check for file.
        return false;
    }
    if (isArgTCorrect(7, argv) != false) {
        setFileArgValue(8);
    }
    return true;
}
/*
 * there are 9 arguments in this function.
 * note: ./csim-ref(0) [-hv](1) -s(2) 4(3) -E(4) 1(5) -b(6) 4(7) -t(8) traces/yi.trace(9).. (10)??null??
 */
bool numArgsIs10(char **argv) {
    setM_Value();
    if (isArgHVCorrect(1, argv) == false) {
        printf("-h or -v was typed incorrectly\n");
        return false;
    }
    if (isArgSCorrect(2, argv) == false) {
        printf("-s was typed incorrectly\n");
        return false;
    }
    if (isArgSCorrect(2, argv) != false) {
        setS_Value(3, argv);
    }
    if (isArgECorrect(4, argv) == false) {
        printf("-E was typed incorrectly\n");
        return false;
    }
    if (isArgECorrect(4, argv) != false) {
        setE_Value(5, argv);
    }
    if (isArgBCorrect(6, argv) == false) {
        printf("-b was typed incorrectly\n");
        return false;
    }
    if (isArgBCorrect(6, argv) != false) {
        setB_Value(7, argv);
    }
    if (isArgTCorrect(8, argv) == false) {
        printf("-t was typed incorrectly\n");
        return false;
    }
    if (isArgTCorrect(8, argv) != false) {
        setFileArgValue(9);
    }
    return true;
}

bool isNumArgs9(int numberOfArguments) {
    if (numberOfArguments == 9) {
        return true;
    }
    return false;
}

bool isNumArgs10(int numberOfArguments) {
    if (numberOfArguments == 10) {
        return true;
    }
    return false;
}

/*
 * strcmp(str1, str2)
This function return values that are as follows −

if Return value < 0 then it indicates str1 is less than str2.

if Return value > 0 then it indicates str2 is less than str1.

if Return value = 0 then it indicates str1 is equal to str2.
 */
bool isArgHVCorrect(int index, char **argv) {
    int compareStrings1 = strcmp(argv[index], "-h");
    int compareStrings2 = strcmp(argv[index], "-v");
    if (compareStrings1 == 0 || compareStrings2 == 0) {
        if (compareStrings1 == 0) {
            printf("-h function needed.\n");
        }
        if (compareStrings2 == 0) {
            printf("-v function needed.\n");
        }
        return true;
    } else return false;
}

bool isArgSCorrect(int index, char **argv) {
    int compareStrings = strcmp(argv[index], "-s");
    if (compareStrings != 0) {
        return false;
    } else return true;
}

bool isArgECorrect(int index, char **argv) {
    int compareStrings = strcmp(argv[index], "-E");
    if (compareStrings != 0) {
        return false;
    } else return true;
}

bool isArgBCorrect(int index, char **argv) {
    int compareStrings = strcmp(argv[index], "-b");
    if (compareStrings != 0) {
        return false;
    } else return true;
}

bool isArgTCorrect(int index, char **argv) {
    int compareStrings = strcmp(argv[index], "-t");
    if (compareStrings != 0) {
        return false;
    } else return true;
}

/*
 * Memory size (M) = 64 bits --> 6 bits because s^m = 2^6 = 64bits therefore. m = 6.
 * Memory BLOCK size = b(2^b) --> example: B = 16  therefore b = 4 because 2^b = 2^4 = 16.
 * Cache size = S * E * B --> 4(bytes per word) * E(line size) * S(sets)
 * Associativity = E example: if E = 1. 1 line per set.
 * Number of sets = S(2^s) example S = 4  therefore s = 2 because 2^s = 2^2 = 4
 * tag = m - s - b. example --> 6 - 2 - 4 = 0.
 */
long set_Value;
long linesPerSet_Value;
long block_Value;
long memory_Value;
int fileArgValue;
long tagBits_Size;

/*
 * Set value
 * Number of sets = S = (2^s) example S = 4  therefore s = 2 because 2^s = 2^2 = 4
 */
void setS_Value(int index, char **argv) {
    int value = atoi(argv[index]);
    set_Value = value;
}

//number of sets
long getS_Value() {
    int numberOfSets = pow(2, set_Value);
    return numberOfSets;
}

//number of set bits
long getS_bitSize() {
    return set_Value;
}

/*
 * number of lines per set. no, manipulation needed.
 * Associativity = E example: if E = 1. 1 line per set.
 */
void setE_Value(int index, char **argv) {
    int value = atoi(argv[index]);
    linesPerSet_Value = value;
}

long getE_Value() {
    return linesPerSet_Value;
}

/*
 * block bits.
 * Memory BLOCK size = b(2^b) --> example: B = 16  therefore b = 4 because 2^b = 2^4 = 16.
 */
void setB_Value(int index, char **argv) {
    int value = atoi(argv[index]);
    block_Value = value;
}

long getB_Value() {
    int numberOfBlocks = pow(2, block_Value);
    return numberOfBlocks;
}

long getB_bitSize() {
    return block_Value;
}


/*
 * Memory size is assumed to be 64bits according to the lab instructions.
 * Memory size (M) = 64 bits --> 6 bits because s^m = 2^6 = 64bits therefore. m = 6.
 * these values will be hardcoded.
 */
void setM_Value() {
    memory_Value = 64;
}

long getM_Value() {
    return memory_Value;
}

void setTag_bitSize() {
    tagBits_Size = 64 - getS_bitSize() - getB_bitSize();
}

long getTag_bitSize() {
    return tagBits_Size;
}

void setFileArgValue(int index) {
    fileArgValue = index;
}

int getFileArgValue() {
    return fileArgValue;
}


/*
 * For future reference on details: check folder --> Fall 2020 --> CS341
 * up to this point all that was done, was check for input arguments.
 * and made setters and getters for the cache. --> M, m , S, s, E, e, B, b , tagbits
 */
/*
 * review: How does a cache work?
 * Cache consists of blocks that contain sets of lines of information.
 * as the cache goes through the lines, code should determine if you
 * have a hit, miss or evict.
 * cache starts with empty lines of information --> 0's
 * as it goes through the sets of lines, the cache will update the set of lines randomly or replace the oldest
 * unused line. this is how hits, misses, or evicts occur.
 * if tag is found within the set. Hit+1
 * else Miss +1
 * evict+1 if there is a Miss and a line needs to be updated with the latest tag within the set.(using LRU or random.)
 *
 * Three things:
 * 1.
 * I = instruction load.
 * L = data load. --> miss and evict if no space is left.
 * S = data store. --> hit or miss and evict if no space is left.
 * M = data modify.  --> evict

 * 2. Trace Examples:
 * M 0421c7f0,4
 * L 04f6b868,8
 * S 7ff0005c8,8
 * Each line denotes one or two memory accesses. The format of each line is:
 * [space]operation address,size

 * 3. The address field specifies a 64-bit hexadecimal memory address.
 * The size field specifies the number of bytes accessed by the operation.
 * NOTE: don’t forget that the address values are given in hexadecimal.

 *4. note:
 * &x == value.
 * *x == address.
 */

char file_operation;
unsigned long file_address;
int file_byteSize;

void setFile_Operation(char file_operation) {
    file_operation = file_operation;
}

char getFile_Operation() {
    return file_operation;
}

void setFile_Address(unsigned long file_address) {
    file_address = file_address;
}

unsigned long getFile_Address() {
    return file_address;
}

void setFile_ByteSize(int file_byteSize) {
    file_byteSize = file_byteSize;
}

int getFile_ByteSize() {
    return file_byteSize;
}

short file_validBit;
unsigned long file_tagBitAddress;
unsigned long file_setBitNumber;
unsigned long file_blockBitNumber;
int countHit;
int countMiss;
int countEvict;

/*
 * valid bit will be checked for each line.
 * set valid bit each time an evict occurs
 * otherwise
 * question. how do you determine if a valid bit is 1 or 0 after initialization?
 */
void setValidBit(int validbit) {
    file_validBit = validbit;
}

int getValidBit() {
    return file_validBit;
}

/*
 * This will be used for tagbits ==> |validbit| |tagbits| |setbit| |blockbits|
 */
void setFile_TagBitsAddress(unsigned long file_address) {
    file_tagBitAddress = file_address >> (64 - getTag_bitSize());
}

unsigned long getFile_TagBitsAddress() {
    return file_tagBitAddress;
}

void setFile_SetBitsAddress(unsigned long file_address) {
    file_setBitNumber = (file_address << getTag_bitSize()) >> (64 - getS_bitSize());
}

unsigned long getFile_SetBitsAddress() {
    return file_setBitNumber;
}

void setFile_BlockBitsAddress(unsigned long file_address) {
    file_blockBitNumber = file_address;
}

unsigned long getFile_BlockBitsAddress() {
    return file_blockBitNumber;
}

struct Cacheline {
    short cacheLine_validBit;
    unsigned long cacheLine_setBitAddress;
    unsigned long cacheLine_tagBitAddress;
    unsigned long cacheLine_blockBitAddress;

};

/*
 * Miss happens when there fails to be a hit.
 */
bool isMiss(char operation, long line_address, long tag_address) {
    bool isMiss = false;
    if(line_address != tag_address){
        countHit++;
        isMiss = true;
    }
    switch (operation) {
        case 'M':
            printf("case M:\n");
            break;
        case 'L':
            printf("case L:\n");
            break;
        case 'S':
            printf("case S:\n");
            break;
    }
    return isMiss;
}

/*
 * Hit happens when the valid bit = 1 and the cacheTag matches the addressTag.
 */
bool isHit(char operation, long line_address, long tag_address) {

    bool isHit = false;

    if(line_address == tag_address){
        countHit++;
        isHit = true;
    }
    switch (operation) {
        case 'M':
            printf("case M:\n");
            break;
        case 'L':
            printf("case L:\n");
            break;
        case 'S':
            printf("case S:\n");
            break;
    }
    return isHit;
}

char *readFile(const char *filename) {

    //read file checks
    long int size = 0;
    FILE *file = fopen(filename, "r");
    if (!file) {
        fputs("File error.\n", stderr);
        return NULL;
    }
    fseek(file, 0, SEEK_END);
    size = ftell(file); //size of file
    rewind(file); // go back to first line.

    char *result = (char *) malloc(size);

    if (!result) {
        fputs("Memory error.\n", stderr);
        fclose(file);
        return NULL;
    }

    struct Cacheline line[getS_Value()][getE_Value()];

    int temp_validBit = 0;
    long temp_tagBitsAddress = 0;
    long temp_setBitsAddress = 0;
    long temp_blockBitsAddress = 0;
    int stored_validBit[size][size];
    long stored_tagBitsAddress[size][size];
    long stored_setBitsAddress[size][size];
    long stored_blockBitsAddress[size][size];

    //initialize cache.
    for (int set = 0; set < getS_Value(); set++) {
        for (int linesPerSet = 0; linesPerSet < getE_Value(); linesPerSet++) {
            line[set][linesPerSet].cacheLine_validBit = 0;
            stored_validBit[set][linesPerSet] = line[set][linesPerSet].cacheLine_validBit;
            line[set][linesPerSet].cacheLine_tagBitAddress = 0;
            stored_tagBitsAddress[set][linesPerSet] = line[set][linesPerSet].cacheLine_tagBitAddress;
            line[set][linesPerSet].cacheLine_setBitAddress = 0;
            stored_setBitsAddress[set][linesPerSet] = line[set][linesPerSet].cacheLine_setBitAddress;
            line[set][linesPerSet].cacheLine_blockBitAddress = 0;
            stored_blockBitsAddress[set][linesPerSet] = line[set][linesPerSet].cacheLine_blockBitAddress;
        }
    }


    char operation;
    unsigned long address;
    int byteSize;
    char countFile;
    //this for loop will iterate each time the while loop finishes.
    //the idea is ..
    //1. iterate through file... forloop--> while(set line, check cache, fill cache. end.) restart. to EOF.

//    for (int readTraceFileLine = 0; readTraceFileLine < size; readTraceFileLine++) {
//        printf("size = %ld\n", size);

    for (countFile = getc(file); countFile != EOF; countFile = getc(file)) {
        while (fscanf(file, " %c %ld,%d", &operation, &address, &byteSize) == 3) {
            printf("file_operation= %c, file_address= %ld, file_byteSize= %d\n", operation, address, byteSize);
            //these will set the values of the file line being read onto functions so they can be manipulated.
            setFile_Operation(operation);
            setFile_Address(address);
            setFile_ByteSize(byteSize);

            //these are the manipulated values to get valid bits, tag bits, set bit number, and block number.
            //valid bit function needs more researching.
            setFile_TagBitsAddress(address);
            setFile_SetBitsAddress(address);
            setFile_BlockBitsAddress(address);
            printf("file_address = %ld, tag_bitaddress = %ld\n", address, getFile_TagBitsAddress());
            printf("s = %ld, E = %ld, b = %ld\n", getS_bitSize(), getE_Value(), getB_bitSize());
            //store and check Cache based on the set and the number of lines.
            temp_tagBitsAddress = getFile_TagBitsAddress();
            temp_setBitsAddress = getFile_SetBitsAddress();
            /*
             * Thoughts -->
             */
            for (int sets = 0; sets < getS_Value(); sets++) {
                for (int linesPerSet = 0; linesPerSet < getE_Value(); linesPerSet++) {
                    if(isMiss(operation, address, stored_tagBitsAddress[sets][linesPerSet]) == true){
                        if(temp_setBitsAddress == stored_setBitsAddress[sets][linesPerSet])
                        line[sets][linesPerSet].cacheLine_validBit = temp_validBit;
                        line[sets][linesPerSet].cacheLine_tagBitAddress = temp_tagBitsAddress;
                        line[sets][linesPerSet].cacheLine_blockBitAddress = temp_blockBitsAddress;
                        printf("validBit = %d, tagBit = %ld, setBit = %ld, blockBit = %ld\n",
                               temp_validBit, temp_tagBitsAddress, temp_setBitsAddress, temp_blockBitsAddress);
                    }
                    else if (isHit(operation, address, stored_tagBitsAddress[sets][linesPerSet]) == true) {
                        stored_validBit[sets][linesPerSet] = 1;
                        line[sets][linesPerSet].cacheLine_validBit = temp_validBit;
                        line[sets][linesPerSet].cacheLine_tagBitAddress = temp_tagBitsAddress;
                        line[sets][linesPerSet].cacheLine_blockBitAddress = temp_blockBitsAddress;
                        printf("validBit = %d, tagBit = %ld, setBit = %ld, blockBit = %ld\n",
                               stored_validBit[sets][linesPerSet],
                               stored_tagBitsAddress[sets][linesPerSet],
                               stored_setBitsAddress[sets][linesPerSet],
                               stored_blockBitsAddress[sets][linesPerSet]);
                    }
                }
            }
        }
    }
    fclose(file);
    printSummary(countHit, countMiss, countEvict);
    return result;
}

void printSummary(int hit, int miss, int evict) {
    printf("hit = %d, miss = %d, evict = %d\n", hit, miss, evict);
}

/*
 * note: arg 0 is program, arg1 is optional therefore..
 * ./csim-ref [-hv] -s 4 -E 1 -b 4 -t traces/yi.trace = 9 arguments.
 * not sure why C is adding an extra arg. but actual count is 9 and 10 args
 * assuming -h and -v can't be used at the same time.
 */
int main(int argc, char **argv) {
    areArgumentsValid(argc, argv);

    if (argc < 2) {
        fputs("Check arguments. ERROR\n", stderr);
        return -1;
    }
    char *result = readFile(argv[getFileArgValue()]);
    if (!result) {
        fputs(result, stdout);
        free(result);
        return -1;
    }
    printf("reached.\n");
    return 0;
}
