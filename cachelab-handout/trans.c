/*
 * Alexander Alvara
 * alvaraa88
 * 11/25/20
 */

/*
 * trans.c - Matrix transpose B = A^T
 *
 * Each transpose function must have a prototype of the form:
 * void trans(int M, int N, int A[N][M], int B[M][N]);
 *
 * A transpose function is evaluated by counting the number of misses
 * on a 1KB direct mapped cache with a block size of 32 bytes.
 */ 
#include <stdio.h>
#include "cachelab.h"

/* note:
 * A = M*N ==> M = rows ==> N = columns.
 * example:
 * | 1 2 |
 * | 3 4 |
 * notice: rows and columns.
 *
 * A_transpose = N * M == N = rows ==> M = columns.
 * example:
 * | 1 3 |
 * | 2 4 |
 * notice: rows ARE columns, columns ARE rows.
 */



int is_transpose(int M, int N, int A[N][M], int B[M][N]);

/* 
 * transpose_submit - This is the solution transpose function that you
 *     will be graded on for Part B of the assignment. Do not change
 *     the description string "Transpose submission", as the driver
 *     searches for that string to identify the transpose function to
 *     be graded. 
 */

/*
 * 3 conditions
 * 32 * 32;
 * 64 * 64;
 * 61 * 67:
 */
char transpose_submit_desc[] = "Transpose submission";

//I tried simply switching indices between ar
void transpose_submit(int M, int N, int A[N][M], int B[M][N])
{
   if(N == 32 & M == 32){
       //transpose_32(M, N, A[N][M], B[M][N]);
       /*note:
        * max size of a matrix = 8; thus 32/4 = 8 .. 64/8 = 8
        */
       for (int A_rows = 0; A_rows < N; A_rows =+ 4) {
           for (int A_columns = 0; A_columns < M; A_columns =+ 4) {
               for (int T_rows = A_rows; T_rows < N + 4; T_rows++) {
                   int new_column = A_columns + T_rows + 1; //this starts a column in a new row.
                   for (int T_columns = new_column; T_columns < M + 4; T_columns++) {

                       B[T_columns][T_rows] = A[T_rows][T_columns];
                   }
               }
           }
       }
   }//end of 32bit
   else if(N == 64 & M == 64){
       //transpose_64(M, N, A[N][M], B[M][N]);
       /*note:
       * max size of a matrix = 8; thus 32/4 = 8 .. 64/8 = 8
       */
       for (int A_rows = 0; A_rows < N; A_rows =+ 8) {
           for (int A_columns = 0; A_columns < M; A_columns =+ 8) {
               for (int T_rows = A_rows; T_rows < N + 8; T_rows++) {
                   int new_column = A_columns + T_rows + 1; //this starts a column in a new row.
                   for (int T_columns = new_column; T_columns < M + 8; T_columns++) {

                       B[T_columns][T_rows] = A[T_rows][T_columns];
                   }
               }
           }
       }
   } else{
       //transpose_random(M, N, A[N][M], B[M][N]);
       /*note:
    * max size of a matrix = 8; thus 32/4 = 8 .. 64/8 = 8
     * for random 61*67 ???
     * 61/8 = 7.625  .. 67/8 = 8.375
    */
       int modRow;
       int modColumn;
       if(M <= 32){
           modRow = M/4;
       }
       if(N <= 32){
           modRow = M/4;
       }
       if(M <= 64){
           modRow = M/8;
       }
       if(N <= 64){
           modRow = M/8;
       }
       for (int A_rows = 0; A_rows < N; A_rows =+ modRow) {
           for (int A_columns = 0; A_columns < M; A_columns =+ modColumn) {
               for (int T_rows = A_rows; T_rows < N + modRow; T_rows++) {
                   int new_column = A_columns + T_rows + 1; //this starts a column in a new row.
                   for (int T_columns = new_column; T_columns < M + modColumn; T_columns++) {
                       B[T_columns][T_rows] = A[T_rows][T_columns];
                   }
               }
           }
       }
   }
}

/* 
 * You can define additional transpose functions below. We've defined
 * a simple one below to help you get started. 
 */ 

/* 
 * trans - A simple baseline transpose function, not optimized for the cache.
 */
char trans_desc[] = "Simple row-wise scan transpose";
void trans(int M, int N, int A[N][M], int B[M][N])
{
    int i, j, tmp;

    for (i = 0; i < N; i++) {
        for (j = 0; j < M; j++) {
            tmp = A[i][j];
            B[j][i] = tmp;
        }
    }    

}

/*
 * registerFunctions - This function registers your transpose
 *     functions with the driver.  At runtime, the driver will
 *     evaluate each of the registered functions and summarize their
 *     performance. This is a handy way to experiment with different
 *     transpose strategies.
 */
void registerFunctions()
{
    /* Register your solution function */
    registerTransFunction(transpose_submit, transpose_submit_desc); 

    /*
     * getting errors
     */
    /* Register any additional transpose functions */
    registerTransFunction(trans, trans_desc);

}

/* 
 * is_transpose - This helper function checks if B is the transpose of
 *     A. You can check the correctness of your transpose by calling
 *     it before returning from the transpose function.
 */
int is_transpose(int M, int N, int A[N][M], int B[M][N])
{
    int i, j;

    for (i = 0; i < N; i++) {
        for (j = 0; j < M; ++j) {
            if (A[i][j] != B[j][i]) {
                return 0;
            }
        }
    }
    return 1;
}